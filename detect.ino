// defines pins numbers
const int ledPin =  12;      // the number of the LED pin
#include <SoftwareSerial.h>

SoftwareSerial serial(2, 3); // Définit les broches RX et TX de la connexion série

const int trigPin = 9;
const int echoPin = 10;
// defines variables
long duration;
int distance;
void setup() {
   Serial.begin(9600); // Initialise la connexion série pour l'affichage de messages de débogage
   serial.begin(9600); // Initialise la connexion série pour communiquer avec Flask

  delay(1000); 
   pinMode(ledPin, OUTPUT);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.print("Distance: ");
}
void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2;

   if (distance < 10) {
    // turn LED on:
    
    digitalWrite(ledPin, HIGH);
     Serial.println("Passage");
    delay( 2000 );
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
    Serial.println("Pas de passage");
  }
  
}
