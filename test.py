from flask import Flask
from threading import Thread
import serial
import sqlite3
import sys

conn = sqlite3.connect('bdd')
cursor = conn.cursor()
app = Flask(__name__)

def read_serial():
    data = None
    while data is not None:
        data = serial.Serial('/dev/ttyUSB0', 9600).readline().decode('ISO-8859-1').strip()
        if data == "Distance: Passage":
            print("Passage detected!",  file=sys.stdout)
            print(data)
            sys.stdout.flush()
            cursor.execute('INSERT INTO passages (passage) VALUES (12)')
            data = None
        else :
            sys.stdout.write(data + "\n")
            sys.stdout.flush()
            print(data, file=sys.stdout)

@app.route('/data')
def get_data():
    
    return "coucou"

if __name__ == '__main__':
    thread = Thread(target=read_serial)
    thread.start()
    app.run()
